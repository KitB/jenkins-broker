# Copyright (c) 2011 Robert Elliot
# All rights reserved.
# 
# Permission is hereby granted, free  of charge, to any person obtaining
# a  copy  of this  software  and  associated  documentation files  (the
# "Software"), to  deal in  the Software without  restriction, including
# without limitation  the rights to  use, copy, modify,  merge, publish,
# distribute,  sublicense, and/or sell  copies of  the Software,  and to
# permit persons to whom the Software  is furnished to do so, subject to
# the following conditions:
# 
# The  above  copyright  notice  and  this permission  notice  shall  be
# included in all copies or substantial portions of the Software.
# 
# THE  SOFTWARE IS  PROVIDED  "AS  IS", WITHOUT  WARRANTY  OF ANY  KIND,
# EXPRESS OR  IMPLIED, INCLUDING  BUT NOT LIMITED  TO THE  WARRANTIES OF
# MERCHANTABILITY,    FITNESS    FOR    A   PARTICULAR    PURPOSE    AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE,  ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


import urllib
from urlparse import urlparse
from brokers import BaseBroker
from jenkinsapi.jenkins import Jenkins as JenkinsAPI

class Jenkins(BaseBroker):

    def handle(self, payload):
        data = JenkinsData(payload)
        if self.should_do_build(data):
            self.do_build(data)

    def should_do_build(self, data):
        if data.module_name:
            return self.module_name_matches_commits(data)
        else:
            data.commit = data.commits[0]
            return True

    def module_name_matches_commits(self, data):
        for commit in data.commits:
            for file in commit['files']:
                file_path = file['file']
                if file_path.startswith(data.module_name):
                    data.commit = commit
                    return True
        else:
            return False

    def do_build(self, data):
        cause= 'Triggered by push of revision ' + str(data.commit['revision']) + ': "' + data.commit['message'] + '" to ' + data.full_url + ' by ' + data.commit['author']
        jenkinsapi = self.get_local((data.endpoint, data.user_id, data.api_token), JenkinsAPI)
        jenkinsapi.get_job(data.project_name).invoke(data.token, params=data.params)


class JenkinsData():
    def __init__(self, payload):
        self.full_url = payload['repository']['website']

        # Strip trailing slashes
        if self.full_url[-1:] == "/":
            self.full_url = self.full_url[0:-1]

        self.full_url = self.full_url + payload['repository']['absolute_url']
        service = payload['service']
        self.project_name = service['project_name']
        self.module_name = service['module_name']
        self.user_id = service['user_id']
        self.api_token = service['api_token']
        self.endpoint = service['endpoint']
        self.params = service['params']

        # Again, strip those slashes
        if self.module_name:
            if self.module_name[-1:] != "/":
                self.module_name = self.module_name + "/"
        self.token = service['token']
        self.commits = payload['commits']
        self.commits.reverse()

payload = {'api_token': '39f74c521dd11e46c009c14ec79f3b70',
 'commits': [{'author': 'Kit Barnes <kit@ninjalith.com>',
   'message': 'Herp the derp',
   'revision': 'DEADBEEF'}],
 'repository': {'absolute_url': '/KitB.stuff', 'website': 'http://blah.com'},
 'service': {'api_token': '39f74c521dd11e46c009c14ec79f3b70',
  'endpoint': 'http://176.34.113.216:8080/',
  'module_name': '',
  'project_name': 'sdp-strategy',
  'params' : {},
  'token': '_strategy',
  'user_id': 'kitb'},
 'userid': 'kitb'}
